from flask import render_template, Blueprint, request, redirect, url_for
from flask_login import login_required, current_user
from flask_uploads import UploadSet, IMAGES

from models.database.repositories import DBManager
from models.entities import Item, Categories
from models.validators import SellItemForm

sell_item = Blueprint('sellItem', __name__)


@sell_item.route("/")
@login_required
def index_sell_item():
    db = DBManager()
    user_items = db.get_current_user_items()
    formValidation = SellItemForm()
    formValidation.itemPrice.data = "0"
    formValidation.itemDescription.data = ""
    formValidation.itemName.data = ""
    formValidation.itemCategory = Categories.PETS.value[1]
    return render_template("sellItem.html", form=formValidation, user_items=user_items)


@sell_item.route('/upload', methods=['GET', 'POST'])
@login_required
def uploadItem():
    photos = UploadSet('photos', IMAGES)
    formData = request.form
    formValidation = SellItemForm(formData)
    if request.method == 'POST' and formValidation.validate():
        if request.files and request.files['itemImage']:
            filename = photos.save(request.files['itemImage'])
            imgUrl = photos.url(filename)
        else:
            imgUrl = 'https://camo.githubusercontent.com/f8ea5eab7494f955e90f60abc1d13f2ce2c2e540/68747470733a2f2f662e636c6f75642e6769746875622e636f6d2f6173736574732f323037383234352f3235393331332f35653833313336322d386362612d313165322d383435332d6536626439353663383961342e706e67'
        if not request.form['itemPrice']:
            request.form['itemPrice'] = 0
        newItem = Item(None, current_user.userId, request.form['itemName'], imgUrl, request.form['itemDescription'],
                       float(request.form['itemPrice']), request.form['itemCategory'])
        db = DBManager()
        db.insert_item(newItem)
        return redirect(url_for('items.index_item', item_id=newItem.itemId))
    db = DBManager()
    user_items = db.get_current_user_items()
    return render_template('sellItem.html', form=formValidation, user_items=user_items)


# @sell_item.route('/deleteItem/<int:item_id>', methods=['DELETE'])
# @login_required
# def deleteItem(item_id):
#     db = DBManager()
#     item = db.get_item(item_id)
#     if item.userId == current_user.userId:
#         pass
