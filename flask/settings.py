from flask import Flask, render_template, jsonify, Blueprint, request, g, redirect, url_for
from flask_login import login_user, logout_user, current_user, login_required
from models.entities import User
from models.validators import UpdateAccountForm
from models.database.repositories import DBManager
from models import login_helper
import hashlib, uuid

settings = Blueprint('settings', __name__)


@settings.route("/")
@login_required
def index_settings():
    return render_template("settings.html", newInfos=current_user, form=UpdateAccountForm())


@settings.route('/update', methods=['POST'])
@login_required
def update():
    formData = request.form
    formValidation = UpdateAccountForm(formData)
    salt = str(uuid.uuid4())

    newUser = User(current_user.userId,
                   current_user.email,
                   hash(formData.get('password') + salt),
                   salt,
                   formData.get('fname'),
                   formData.get('lname'),
                   formData.get('usrtel'),
                   formData.get('address'),
                   formData.get('zip'),
                   formData.get('country'),
                   formData.get('city'))

    if formValidation.validate():
        if not formData.get('password') == formData.get('passwordConfirm'):
            formValidation.password.errors.append("Le mot de passe a mal été entré lors de sa confirmation")
            return render_template("settings.html", newInfos=newUser, form=formValidation)

        db = DBManager()
        db.update_user(newUser)
        login_user(newUser)
        login_helper.login_user(newUser)

    else:
        return render_template("settings.html", newInfos=newUser, form=formValidation)

    return redirect(url_for('index'))


def hash(password):
    return hashlib.sha256(password.encode('UTF-8')).hexdigest()
