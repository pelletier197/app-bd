function httpGet(route, handler) {
    $.ajax({
        url: route,
        contentType: 'application/json',
        method: 'GET'
    }).done(function (response) {
        handler(response)
    }).catch(function (reason) {
        if (reason.status === 401) {
            window.location = '/login';
        }
    });
}

function httpPut(route, data, handler) {
    $.ajax({
        url: route,
        contentType: 'application/json',
        data: JSON.stringify(data),
        method: 'PUT'
    }).done(function (response) {
        handler(response)
    }).catch(function (reason) {
        if (reason.status === 401) {
            window.location = '/login';
        }
    });
    ;
}

function httpPost(route, data, handler) {
    $.ajax({
        url: route,
        contentType: 'application/json',
        data: JSON.stringify(data),
        method: 'POST'
    }).done(function (response) {
        handler(response)
    }).catch(function (reason) {
        if (reason.status === 401) {
            window.location = '/login';
        }
    });
    ;
}

function httpDelete(route, handler) {
    $.ajax({
        url: route,
        contentType: 'application/json',
        method: 'DELETE'
    }).done(function (response) {
        handler(response)
    }).catch(function (reason) {
        if (reason.status === 401) {
            window.location = '/';
        }
    });
    ;
}