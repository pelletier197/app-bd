function addToCart(route) {
    httpPut(route, {}, function (data) {
        $('#my-cart').html('Mon panier (' + data.itemCount + ')');
        $.toast({
            heading: 'Success',
            text: "L'item a été ajouté au panier avec succès !",
            icon: 'success',
            position: 'top-left',
            loader: false
        })
    });
}

function removeFromCart(route) {
    httpDelete(route, function (data) {
        $('#my-cart').html('Mon panier (' + data.itemCount + ')');
        $.toast({
            heading: 'Success',
            text: "L'item a été supprimé du panier avec succès !",
            icon: 'success',
            position: 'top-left',
            loader: false
        })
        window.location.reload();
    });
}