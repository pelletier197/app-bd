import os
from flask import Flask, Session, render_template, url_for
from flask_login import LoginManager, current_user
from flask_uploads import UploadSet, IMAGES, configure_uploads

from items import item_view
from login import login
from orders import orders
from cart import cart
from search import search
from settings import settings
from sellItem import sell_item

from models.entities import Categories
from models import login_helper
from models.database.repositories import DBManager

import random

app = Flask(__name__)
app.register_blueprint(item_view, url_prefix='/item')
app.register_blueprint(login, url_prefix='/login')
app.register_blueprint(orders, url_prefix='/orders')
app.register_blueprint(cart, url_prefix='/cart')
app.register_blueprint(search, url_prefix='/search')
app.register_blueprint(settings, url_prefix='/settings')
app.register_blueprint(sell_item, url_prefix='/sell-item')

app.secret_key = 'jchh204fuiweyhvogw0f9pqo3ufhbvq8902olkef'
app.config['SESSION_TYPE'] = 'filesystem'

sess = Session()

login_manager = LoginManager()
login_manager.init_app(app)


@app.context_processor
def inject_categories():
    return dict(categories=Categories.list(), user=current_user)


@app.after_request
def add_header(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '-1'
    print('REMOVING HEADER')
    return response


photos = UploadSet('photos', IMAGES)

app.config['UPLOADED_PHOTOS_DEST'] = 'images/items'
configure_uploads(app, photos)


def load_banner_images():
    images = os.listdir(os.path.join(os.curdir, 'static', 'banner'))
    texts = [os.path.splitext(x)[0] for x in images]

    result = []

    for i in range(len(texts)):
        result.append({'text': texts[i], 'url': url_for('static', filename='banner/' + images[i])})

    return result


@app.route("/")
def index():
    db = DBManager()

    recent = db.get_recent_items()

    chosen_category = random.choice(Categories.list())
    category = db.get_items_by_category(chosen_category)
    if len(category) >= 6:
        category = random.sample(category, 6)

    popular = db.get_popular_products()

    return render_template("home.html", recent=recent, category=category, popular=popular,
                           chosen_category=chosen_category, banner=load_banner_images())


@login_manager.user_loader
def load_user(user_id):
    user = login_helper.logged_in_users.get(user_id)

    if user is not None:
        return user
    else:
        db = DBManager()
        return db.get_user_by_id(user_id)


if __name__ == "__main__":
    app.run()
