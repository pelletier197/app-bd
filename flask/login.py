from flask import Flask, render_template, jsonify, Blueprint, request, g, redirect, url_for
from flask_login import login_user, logout_user, current_user, login_required
from models.entities import User
from models.validators import SignUpForm, SignInForm
from models.database.repositories import DBManager
from models import login_helper
import hashlib, uuid

login = Blueprint('login', __name__)


@login.route("/")
def index_login():
    user = User("", "", "", "", "", "", "", "", "", "", "")
    return render_template("login.html", user=user, form=SignUpForm(), loginType="login")


@login.route('/sign-up', methods=['POST'])
def sign_up():
    formData = request.form
    formValidation = SignUpForm(formData)
    salt = str(uuid.uuid4())

    user = User(None,
                formData.get('email'),
                hash(formData.get('password') + salt),
                salt,
                formData.get('fname'),
                formData.get('lname'),
                formData.get('usrtel'),
                formData.get('address'),
                formData.get('zip'),
                formData.get('country'),
                formData.get('city'))

    if formValidation.validate():
        if not formData.get('password') == formData.get('passwordConfirm'):
            formValidation.password.errors.append("Le mot de passe a mal été entré lors de sa confirmation")
            return render_template("login.html", user=user, form=formValidation, loginType="create")

        db = DBManager()
        if not db.exists_user(user.email):
            db.insert_user(user)
        else:
            formValidation.email.errors.append("L'adresse email entrée appartient déjà à un autre compte")
            return render_template("login.html", user=user, form=formValidation, loginType="create")
    else:
        return render_template("login.html", user=user, form=formValidation, loginType="create")

    login_user(user)
    login_helper.login_user(user)
    user.cart_count = 0
    
    return redirect(url_for('index'))


@login.route('/sign-in', methods=['POST'])
def log_in():
    formValidation = SignInForm(request.form)
    fallBackUser = User("", request.form.get('emailLog'), "", "", "", "", "", "", "", "", "")

    if not (formValidation.validate()):
        return render_template("login.html", user=fallBackUser, form=formValidation, loginType="login")

    email = request.form.get('emailLog')
    password = request.form.get('passwordLog')

    db = DBManager()
    if db.exists_user(email):
        salt = db.get_user_salt(email)
        user = db.get_user(email, hash(password + salt))
    else:
        user = None

    if user is None:
        formValidation.emailLog.errors.append("Les informations de connection sont invalides")
        return render_template("login.html", user=fallBackUser, form=formValidation, loginType="login")

    login_user(user)
    login_helper.login_user(user)

    return redirect(url_for('index'))


@login.route('/logout')
@login_required
def logout():
    print('HI', current_user, current_user.is_authenticated, current_user.get_id())
    logout_user()
    login_helper.logout_user(current_user.get_id())
    return redirect(url_for('login.index_login'))


def hash(password):
    return hashlib.sha256(password.encode('UTF-8')).hexdigest()
