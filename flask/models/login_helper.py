logged_in_users = dict()


def is_logged_in(userId):
    global logged_in_users
    return userId in logged_in_users


def login_user(user):
    user.is_authenticated = True
    global logged_in_users
    logged_in_users[user.userId] = user


def logout_user(userId):
    global logged_in_users
    usr = logged_in_users.get(userId)
    if usr is not None:
        usr.is_authenticated = False
        del logged_in_users[userId]
