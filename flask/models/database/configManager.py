import configparser
config = configparser.ConfigParser()
config.read('app.config')

default = config['DEFAULT']


def get_db_host():
    return default['DB_HOST']


def get_db_user():
    return default['DB_USER']


def get_db_password():
    return default['DB_PASSWORD']


def get_db_name():
    return default['DB_NAME']
