import pymysql.cursors
from models.database import configManager
from typing import Tuple, List, Dict


class MysqlConnector:

    def __init__(self):
        # Connect to the database
        print("Opening connector automatically")
        # print(configManager.get_db_password())
        print(configManager.get_db_name())
        self.connection = pymysql.connect(host=configManager.get_db_host(),
                                          user=configManager.get_db_user(),
                                          password=configManager.get_db_password(),
                                          db=configManager.get_db_name(),
                                          charset='utf8mb4',
                                          cursorclass=pymysql.cursors.DictCursor)

    def __del__(self):
        print("Close connector automatically")
        self.connection.close()

    def execute_query(self, query: str, params: Tuple = tuple()) -> List:
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query, params)
                data = cursor.fetchall()
            self.connection.commit()
            return data
        except Exception as expt:
            self.connection.rollback()
            raise expt

    def execute_query_one_entry(self, query: str, params: Tuple = tuple()) -> Dict:
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query, params)
                data = cursor.fetchone()
            self.connection.commit()
            return data
        except Exception as expt:
            self.connection.rollback()
            raise expt

    def execute_non_query(self, query: str, params: Tuple = tuple()):
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query, params)
            self.connection.commit()
        except Exception as expt:
            self.connection.rollback()
            raise expt
