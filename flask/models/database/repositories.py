from models.database.dbConnector import MysqlConnector
from models.entities import *
from typing import Union, Dict
from datetime import datetime
from flask_login import current_user


class DBManager:
    def __init__(self):
        print('Creating connector')
        self.connector = MysqlConnector()

    def __del__(self):
        # Ensures connection is freed
        print('Suppressing connector')
        self.connector = None

    def close_connection(self):
        print('Connector closed')
        self.connector = None

    # User queries
    def get_user_salt(self, userMail: str) -> str:
        result = self.connector.execute_query_one_entry(SELECT_PASSWORD_SALT, (userMail,))
        return None if result is None else result['password_salt']

    def get_user(self, userMail: str, passwordHash: str) -> Union[User, None]:
        user_data = self.connector.execute_query_one_entry(SELECT_USER, (userMail, passwordHash))

        if user_data is None:
            return None

        user = self.__read_user(user_data)
        user.cart_count = self.connector.execute_query_one_entry(SELECT_ITEM_COUNT_IN_CART, (user.userId,))[
            'cart_count']

        return user

    def get_user_by_id(self, user_id: int) -> Union[User, None]:
        user_data = self.connector.execute_query_one_entry(SELECT_USER_BY_ID, (user_id,))

        if user_data is None:
            return None

        user = self.__read_user(user_data)
        user.cart_count = self.connector.execute_query_one_entry(SELECT_ITEM_COUNT_IN_CART, (user_id,))['cart_count']

        return user

    def insert_user(self, user: User):
        self.connector.execute_non_query(INSERT_USER, (
            user.email, user.passwordHash, user.passwordSalt, user.firstName, user.lastName, user.phoneNumber,
            user.address, user.zipCode, user.country, user.city))

        # Sets the user's ID
        user.userId = self.connector.execute_query_one_entry(SELECT_LAST_USER)['user_id']

    def exists_user(self, email: str) -> bool:
        data = self.connector.execute_query_one_entry(EXISTS_USER, (email,))
        return data['cpt'] > 0

    def update_user(self, newInfos: User):
        self.connector.execute_non_query(UPDATE_USER, (newInfos.passwordHash,
                                                       newInfos.passwordSalt,
                                                       newInfos.firstName,
                                                       newInfos.lastName,
                                                       newInfos.phoneNumber,
                                                       newInfos.address,
                                                       newInfos.zipCode,
                                                       newInfos.country,
                                                       newInfos.city,
                                                       newInfos.email))

    @staticmethod
    def __read_user(kv: Dict) -> User:
        return User(kv['user_id'], kv['email'], kv['password_hash'],
                    kv['password_salt'],
                    kv['firstname'], kv['lastname'], kv['phone_number'], kv['address'],
                    kv['zip_code'], kv['country'], kv['city'])

    # Item queries
    def insert_item(self, item: Item):
        self.connector.execute_non_query(INSERT_ITEM, (
            item.userId, item.name, item.previewLocation, item.description, item.price, item.category.value[0]
        ))
        # Sets the item's ID
        item.itemId = self.connector.execute_query_one_entry(SELECT_LAST_ITEM)['item_id']

    def get_recent_items(self, qty: int = 8) -> List[Item]:
        result = self.connector.execute_query(SELECT_RECENT_ITEMS, (qty,))
        return [self.__read_item(kv) for kv in result]

    def get_popular_products(self, qty: int = 16):
        result = self.connector.execute_query(SELECT_POPULAR_PRODUCTS, (qty,))
        return [self.__read_item(kv) for kv in result]

    def get_items_by_category(self, category: Categories) -> List[Item]:
        result = self.connector.execute_query(SELECT_ITEM_BY_CATEGORY, category.value[0])
        items = [self.__read_item(kv) for kv in result]
        for i in range(len(items)):
            if result[i]['stars'] is not None:
                items[i].stars = int(result[i]['stars'])
            else:
                items[i].stars = 0
        return items

    def get_item_search(self, searchKey: str) -> List[Item]:
        searchKey = '%' + searchKey + '%'
        result = self.connector.execute_query(SELECT_ITEM_BY_SEARCH, (searchKey, searchKey))
        items = [self.__read_item(kv) for kv in result]
        for i in range(len(items)):
            if result[i]['stars'] is not None:
                items[i].stars = int(result[i]['stars'])
            else:
                items[i].stars = 0
        return items

    def get_item(self, item_id: int) -> Item:
        result = self.connector.execute_query(SELECT_ITEM_BY_ID, (item_id,))
        return self.__read_item(result[0])

    def get_current_user_items(self) -> List[Item]:
        result = self.connector.execute_query(SELECT_ITEM_BY_USER, current_user.userId)
        items = [self.__read_item(kv) for kv in result]
        return items

    @staticmethod
    def __read_item(kv: Dict):
        return Item(kv['item_id'], kv['user_id'], kv['name'], kv['preview_location'], kv['description'],
                    kv['item_price'], kv['category'])

    # Review queries
    def insert_review(self, review: Review):
        self.connector.execute_non_query(INSERT_REVIEW, (
            review.userId, review.itemId, review.comment, review.stars, datetime.now()))

    def get_reviews_by_item_id(self, item_id: int) -> List[Review]:
        result = self.connector.execute_query(SELECT_REVIEWS_BY_ID, (item_id,))
        reviews = [self.__read_review(kv) for kv in result]

        for review in reviews:
            user = self.get_user_by_id(review.userId)
            review.username = '{0} {1}'.format(user.firstName, user.lastName)

        return reviews

    @staticmethod
    def __read_review(kv: Dict):
        review = Review(kv['user_id'], kv['item_id'], kv['comment'], kv['stars'])
        review.date = 'Écrit le {0}'.format(kv['date'])

        return review

    # Cart
    def insert_item_in_cart(self, user_id: int, item_id: int):
        self.connector.execute_non_query(INSERT_ITEM_IN_CART, (user_id, item_id))

    def get_items_in_cart(self, user_id: int):
        ids = self.connector.execute_query(SELECT_ITEMS_IN_CART, (user_id,))
        return list(map(lambda x: self.get_item(x), map(lambda x: x['item_id'], ids)))

    def remove_item_in_cart(self, user_id: int, item_id: int):
        self.connector.execute_non_query(DELETE_ITEMS_IN_CART, (user_id, item_id))

    def clear_cart(self, user_id: int):
        self.connector.execute_non_query(DELETE_ALL_ITEMS_IN_CART, (user_id,))

    # Orders
    def create_order(self, user_id: int) -> int:
        self.connector.execute_non_query(INSERT_ORDER, (user_id,))
        return self.connector.execute_query_one_entry(SELECT_LAST_ORDER)['order_id']

    def add_item_in_order(self, itemId, orderId: int):
        self.connector.execute_non_query(INSERT_ITEM_IN_ORDER, (orderId, itemId))

    def get_orders_from_user(self, user_id: int) -> List[Order]:
        resultIds = self.connector.execute_query(SELECT_ORDERS_FOR_USER, (user_id,))
        orders = [self.__read_orders_id(kv) for kv in resultIds]

        for order in orders:
            total = 0
            result = self.connector.execute_query(SELECT_ORDER_FROM_ID, (order.orderId,))
            [order.add_item(self.__read_order_item(kv)) for kv in result]
            for item in order.items:
                total += item.fullItem.price

            order.total = '{:,.2f}'.format(total)
            order.nbOfItem = len(order.items)
        return orders

    @staticmethod
    def __read_orders_id(kv: Dict):
        return Order(kv['order_id'], kv['user_id'], [])

    def __read_order_item(self, kv: Dict):
        return OrderItem(kv['item_id'], ItemStatus.PAID_AND_DELIVERED, self.get_item(kv['item_id']))


# User queries
INSERT_USER = """
    INSERT INTO users (
    email, password_hash, password_salt, firstname, lastname, 
    phone_number, address, zip_code, country, city
    ) VALUES (
        %s, %s, %s, %s, %s, 
        %s, %s, %s, %s, %s
    )"""

SELECT_LAST_USER = """
    SELECT LAST_INSERT_ID() AS user_id
    FROM users
"""

EXISTS_USER = """
    SELECT
      COUNT(email) AS cpt
    FROM users
    WHERE email = %s
"""

SELECT_PASSWORD_SALT = """
    SELECT 
      password_salt 
    FROM users 
    WHERE email = %s
"""

SELECT_USER = """
    SELECT * 
    FROM users 
    WHERE email = %s AND  password_hash = %s
"""

SELECT_USER_BY_ID = """
    SELECT *
    FROM users
    WHERE user_id = %s
"""

UPDATE_USER = """
    UPDATE users 
    SET password_hash=%s, password_salt=%s, firstname=%s, 
    lastname=%s, phone_number=%s, address=%s, 
    zip_code=%s, country=%s, city=%s
    WHERE email = %s
"""

# Item queries
INSERT_ITEM = """
    INSERT INTO items (user_id, name, preview_location, description, item_price, category)
    VALUES (
      %s, %s, %s, %s, %s, %s
    )
"""
SELECT_LAST_ITEM = """
    SELECT LAST_INSERT_ID() AS item_id
    FROM items
"""

SELECT_RECENT_ITEMS = """
    SELECT * 
    FROM items
    ORDER BY item_id DESC
    LIMIT %s
"""

SELECT_ITEM_BY_CATEGORY = """
    SELECT *
    FROM items I
    LEFT JOIN (SELECT item_id AS iid, AVG(stars) AS stars FROM reviews GROUP BY iid) R ON R.iid = I.item_id
    WHERE category = %s
    ORDER BY item_id DESC
"""

SELECT_ITEM_BY_SEARCH = """
    SELECT *
    FROM items I
    LEFT JOIN (SELECT item_id AS iid, AVG(stars) AS stars FROM reviews GROUP BY iid) R ON R.iid = I.item_id
    WHERE name LIKE %s OR description LIKE %s
    ORDER BY item_id DESC
"""

SELECT_ITEM_BY_ID = """
    SELECT *
    FROM items
    WHERE item_id = %s
"""
SELECT_ITEM_BY_USER = """
    SELECT *
    FROM items 
    WHERE user_id = %s
"""
SELECT_POPULAR_PRODUCTS = """
   SELECT *
   FROM items i INNER JOIN 
   (SELECT item_id AS iid, count(*) AS magnitude
   FROM order_list 
   GROUP BY iid
   ORDER BY magnitude DESC
   LIMIT %s ) o ON o.iid = i.item_id
"""
# Review queries
INSERT_REVIEW = """
    INSERT INTO reviews (user_id, item_id, comment, stars, date) 
    VALUES (%s, %s, %s, %s, %s)
"""

SELECT_REVIEWS_BY_ID = """
    SELECT * 
    FROM reviews
    WHERE item_id = %s
    ORDER BY date DESC
"""

# Cart queries
SELECT_ITEM_COUNT_IN_CART = """
    SELECT count(*) AS cart_count
    FROM cart_items WHERE user_id = %s

"""

INSERT_ITEM_IN_CART = """
    INSERT INTO cart_items (
      user_id,
      item_id
    ) VALUES (
      %s,
      %s
    )
"""

SELECT_ITEMS_IN_CART = """
    SELECT item_id 
    FROM cart_items 
    WHERE user_id = %s
"""

DELETE_ITEMS_IN_CART = """
    DELETE FROM cart_items 
    WHERE user_id = %s AND item_id = %s
"""

DELETE_ALL_ITEMS_IN_CART = """
    DELETE FROM cart_items 
    WHERE user_id = %s 
"""

# Orders
INSERT_ORDER = """
    INSERT INTO orders (
      user_id
    ) VALUES (
      %s
    )
"""

SELECT_LAST_ORDER = """
    SELECT LAST_INSERT_ID() AS order_id
    FROM orders
"""

INSERT_ITEM_IN_ORDER = """
    INSERT INTO order_list (
      order_id,
      item_id
    ) VALUES (
      %s,
      %s
    )
"""

SELECT_ORDERS_FOR_USER = """
    SELECT order_id, user_id 
    FROM orders
    WHERE user_id = %s
"""

SELECT_ORDER_FROM_ID = """
    SELECT order_id, item_id 
    FROM order_list
    WHERE order_id = %s
"""
