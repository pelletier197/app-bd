from wtforms import Form, validators, StringField, FileField
import re

regexLettersOnly = '[\w\s]+$'


class SignUpForm(Form):
    fname = StringField('fname',
                        validators=[validators.required(message="Le champ prénom est obligatoire"),
                                    validators.length(max=45,
                                                      message="Le champ prénom peut avoir une longueur maximale de 45 caractères"),
                                    validators.regexp(regexLettersOnly,
                                                      message="Le prénom ne peut contenir que des lettres")])

    lname = StringField('lname',
                        validators=[validators.required(message="Le champ nom est obligatoire"),
                                    validators.length(max=45,
                                                      message="Le champ nom peut avoir une longueur maximale de 254 caractères"),
                                    validators.regexp(regexLettersOnly,
                                                      message="Le nom ne peut contenir que des lettres")])

    usrtel = StringField('usrtel',
                         validators=[validators.regexp("1?\\d{10}$",
                                                       message="Le numéro de téléphone doit être composé de 10 chiffres (ou 11 si il commence par un 1)."),
                                     validators.required(message="Le champ numéro de téléphone est obligatoire")])
    address = StringField('address',
                          validators=[validators.required(message="Le champ addresse est obligatoire")])
    zip = StringField('zip',
                      validators=[validators.required(message="Le champ code postal est obligatoire"),
                                  validators.regexp(
                                      '[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ]\s?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]',
                                      message="Le code postal n'est pas valide.",
                                      flags=re.IGNORECASE),
                                  validators.length(min=6,
                                                    max=7,
                                                    message="Le code postal doit respecter le format 'X1X 1X1'.")])
    country = StringField('country',
                          validators=[validators.required(message="Le champ pays est obligatoire")])
    city = StringField('city',
                       validators=[validators.required(message="Le champ ville est obligatoire"),
                                   validators.regexp(regexLettersOnly,
                                                     message="La ville ne peut contenir que des lettres.")])

    email = StringField('email',
                        validators=[validators.required(message="Le champ adresse email est obligatoire"),
                                    validators.email(message="Veuillez entrer une adresse courriel valide"),
                                    validators.length(max=254,
                                                      message="Le courriel peut avoir une longueur maximale de 254 caractères")])

    password = StringField('password',
                           validators=[validators.required(message="Le champ mot de passe est obligatoire"),
                                       validators.length(min=6,
                                                         max=15,
                                                         message="Le mot de passe doit avoir entre 6 et 15 caractères")])

    passwordConfirm = StringField('passwordConfirm',
                                  validators=[validators.required(
                                      message="Le champ de confirmation de mot de passe est obligatoire")])


class UpdateAccountForm(Form):
    fname = StringField('fname',
                        validators=[validators.required(message="Le champ prénom est obligatoire"),
                                    validators.length(max=45,
                                                      message="Le champ prénom peut avoir une longueur maximale de 45 caractères"),
                                    validators.regexp(regexLettersOnly,
                                                      message="Le prénom ne peut contenir que des lettres")])

    lname = StringField('lname',
                        validators=[validators.required(message="Le champ nom est obligatoire"),
                                    validators.length(max=45,
                                                      message="Le champ nom peut avoir une longueur maximale de 254 caractères"),
                                    validators.regexp(regexLettersOnly,
                                                      message="Le nom ne peut contenir que des lettres")])

    usrtel = StringField('usrtel',
                         validators=[validators.regexp("1?\\d{10}$",
                                                       message="Le numéro de téléphone doit être composé de 10 chiffres (ou 11 si il commence par un 1)."),
                                     validators.required(message="Le champ numéro de téléphone est obligatoire")])
    address = StringField('address',
                          validators=[validators.required(message="Le champ addresse est obligatoire")])
    zip = StringField('zip',
                      validators=[validators.required(message="Le champ code postal est obligatoire"),
                                  validators.regexp(
                                      '[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ]\s?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]',
                                      message="Le code postal n'est pas valide.",
                                      flags=re.IGNORECASE),
                                  validators.length(min=6,
                                                    max=7,
                                                    message="Le code postal doit respecter le format 'X1X 1X1'.")])
    country = StringField('country',
                          validators=[validators.required(message="Le champ pays est obligatoire")])
    city = StringField('city',
                       validators=[validators.required(message="Le champ ville est obligatoire"),
                                   validators.regexp(regexLettersOnly,
                                                     message="La ville ne peut contenir que des lettres.")])

    password = StringField('password',
                           validators=[validators.required(message="Le champ mot de passe est obligatoire"),
                                       validators.length(min=6,
                                                         max=15,
                                                         message="Le mot de passe doit avoir entre 6 et 15 caractères")])

    passwordConfirm = StringField('passwordConfirm',
                                  validators=[validators.required(
                                      message="Le champ de confirmation de mot de passe est obligatoire")])


class SignInForm(Form):
    emailLog = StringField('emailLog',
                           validators=[validators.required(message="Le champ adresse email est obligatoire"),
                                       validators.email(message="Veuillez entrer une adresse courriel valide")])

    passwordLog = StringField('passwordLog',
                              validators=[validators.required(message="Le champ mot de passe est obligatoire")])


class SellItemForm(Form):
    itemName = StringField('itemName', validators=[validators.required(message="L'item doit avoir un nom"),
                                                   validators.length(min=1, max=100,
                                                                     message="Le nom de l'item peut contenir entre 1 et 100 caractères")])
    itemPrice = StringField('itemName', validators=[validators.required(message="L'item doit avoir un prix")])
    itemCategory = StringField('itemCategory', validators=[validators.required(message="L'item doit avoir une catégorie")])
    itemDescription = StringField('itemDescription', validators=[validators.required(message="L'item doit avoir une description"), validators.length(min=1, max=5000, message="La description peut contenir entre 1 et 5000 caractères")])
