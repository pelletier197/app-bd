from enum import Enum
from typing import List


class Categories(Enum):
    PETS = 1, 'Animaux', 'fax-dog-face'
    TOYS = 2, 'Jouets', 'fax-microphone'
    BABY = 3, 'Enfants', 'fax-baby'
    ELECTRONICS = 4, 'Électronique', 'fax-headphone'
    VIDEO_GAME = 5, 'Jeux videos', 'fax-video-game'
    HOME_KITCHEN = 6, 'Maison et cuisine', 'fax-house-building'
    TOOLS_GARDEN = 7, 'Outils et jardin', 'fax-hammer'
    BEAUTY_HEALTH = 8, 'Santé et beauté', 'fa-medkit'
    CLOTHING = 9, 'Vêtements', 'fax-t-shirt'
    SHOES = 10, 'Souliers', 'fax-athletic-shoe'
    JEWELRY = 11, 'Bijoux', 'fax-ring',
    SPORTS_OUTDOORS = 12, 'Sports et extérieur', 'fax-table-tennis-paddle-and-ball'
    MUSIC = 13, 'Musique', 'fax-guitar'

    @staticmethod
    def from_value(value):
        for item in Categories:
            if value in item.value:
                return item
        raise ValueError("%r is not a valid %s" % (value, Categories.__name__))

    @staticmethod
    def list():
        return [e for e in Categories]


class ItemStatus(Enum):
    PAID_AND_DELIVERED = 'P'


class User:
    def __init__(self, userId: int, email: str, passwordHash: str, passwordSalt: str, firstName: str, lastName: str,
                 phoneNumber: str,
                 address: str, zipCode: str, country: str, city: str):
        self.userId = userId
        self.email = email
        self.passwordHash = passwordHash
        self.passwordSalt = passwordSalt
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.address = address
        self.zipCode = zipCode
        self.country = country
        self.city = city
        self.is_authenticated = False
        self.is_active = True
        self.is_anonymous = False

    def get_id(self):
        return self.userId


class Item:
    def __init__(self, itemId: int, userId: int, name: str, previewLocation: str, description: str, price: float,
                 category: int):
        self.itemId = itemId
        self.userId = userId
        self.name = name
        self.previewLocation = previewLocation
        self.description = description
        self.price = price
        self.priceDisplay = '{:,.2f}'.format(price)
        self.category = Categories.from_value(category)

    def __str__(self) -> str:
        return 'item id = ' + str(self.itemId)


class WishList:
    def __init__(self, wishListId: int, userId: int, wishListName: str, items: List[Item] = []):
        self.wishListId = wishListId
        self.userId = userId
        self.wishListName = wishListName
        self.items = items

    def add_item(self, item: Item):
        self.items.append(item)

    def remove_item_from_id(self, itemId: int):
        self.items = filter(lambda item: item.itemId != itemId, self.items)


class OrderItem:
    def __init__(self, itemId: int, itemStatus: ItemStatus, fullItem: Item):
        self.itemId = itemId
        self.itemStatus = itemStatus
        self.fullItem = fullItem


class Order:
    def __init__(self, orderId: int, userId: int, items: List[OrderItem] = [], total: float = 0):
        self.orderId = orderId
        self.userId = userId
        self.items = items
        self.total = total
        self.nbOfItem = len(items);

    def add_item(self, item: OrderItem):
        self.items.append(item)
        self.nbOfItem += 1

    def set_total(self, total: float):
        self.total = total

    def remove_item_from_id(self, itemId: int):
        self.items = filter(lambda item: item.itemId != itemId, self.items)
        self.nbOfItem -= 1


class Review:
    def __init__(self, userId: int, itemId: int, comment: str, stars: int):
        self.userId = userId
        self.itemId = itemId
        self.comment = comment
        self.stars = stars
