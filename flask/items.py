from flask import Flask, render_template, jsonify, Blueprint, jsonify, request
from flask_login import login_required, current_user
from models.database.repositories import DBManager
from models.entities import Review

item_view = Blueprint('items', __name__)


@item_view.route("/<int:item_id>")
def index_item(item_id):
    db = DBManager()
    item = db.get_item(item_id)
    item.reviews = db.get_reviews_by_item_id(item_id)
    item.canComment = current_user.is_authenticated and not any(x.userId == current_user.userId for x in item.reviews)

    return render_template("itemView.html", item=item)


@item_view.route('/<int:item_id>/reviews', methods={'PUT'})
@login_required
def insert_review(item_id):
    data = request.get_json()
    stars = data['stars']
    text = data['text']

    db = DBManager()
    db.insert_review(Review(current_user.userId, item_id, text, stars))

    return jsonify({'username': current_user.firstName + ' ' + current_user.lastName, 'comment': text, 'stars': stars})
