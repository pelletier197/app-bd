from itertools import groupby

from flask import Flask, render_template, jsonify, Blueprint
from flask_login import login_required, current_user

from models.database.repositories import DBManager

orders = Blueprint('orders', __name__)


@orders.route("/")
@login_required
def index_orders():
    db = DBManager()

    orders = db.get_orders_from_user(current_user.userId)

    return render_template("orders.html", orders= orders, nbOfOrders=len(orders))
