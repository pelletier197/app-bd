from flask import Flask, render_template, jsonify, Blueprint, redirect, url_for
from flask_login import login_required, current_user
from models.database.repositories import DBManager
from itertools import groupby

cart = Blueprint('cart', __name__)


@cart.route("/")
@login_required
def index_cart():
    db = DBManager()
    items = db.get_items_in_cart(current_user.userId)
    items = [list(v) for l, v in groupby(items, lambda x: x.itemId)]
    items = [{'count': len(x), 'item': x[0], 'total': len(x) * x[0].price,
              'priceDetails': "{0} x {1} = {2}".format(len(x), '{:,.2f}'.format(x[0].price),
                                                       '{:,.2f}'.format(x[0].price * len(x)))} for x in items]
    totalPrice = sum(map(lambda x: x['total'], items))
    return render_template("cart.html", items=items, totalPrice='{:,.2f}'.format(totalPrice))


@cart.route("/<int:item_id>", methods={'PUT'})
@login_required
def add_to_cart(item_id):
    db = DBManager()
    db.insert_item_in_cart(current_user.userId, item_id)
    current_user.cart_count += 1
    return jsonify({'itemCount': current_user.cart_count})


@cart.route("/<int:item_id>", methods={'DELETE'})
@login_required
def remove_from_cart(item_id):
    db = DBManager()
    db.remove_item_in_cart(current_user.userId, item_id)
    current_user.cart_count = len(db.get_items_in_cart(current_user.userId))
    return jsonify({'itemCount': current_user.cart_count})


@cart.route("/buy")
@login_required
def confirm_buy():
    db = DBManager()
    orderId = db.create_order(current_user.userId)
    items = db.get_items_in_cart(current_user.userId)

    for item in items:
        db.add_item_in_order(item.itemId, orderId)

    db.clear_cart(current_user.userId)
    current_user.cart_count = 0
    return redirect(url_for('index'))
