from flask import Flask, render_template, jsonify, Blueprint, request
from models.entities import Categories
from models.database.repositories import DBManager

search = Blueprint('search', __name__)


@search.route("/", methods={'POST'})
def index_search():
    searchKey = request.form['search']
    db = DBManager()
    results = db.get_item_search(searchKey)
    return render_template("searchResults.html", results=results, text=searchKey)


@search.route('/categories/<int:category>')
def search_by_category(category):
    category = Categories.from_value(category)
    db = DBManager()
    results = db.get_items_by_category(category)
    return render_template("searchResults.html", results=results, text=category.value[1])
